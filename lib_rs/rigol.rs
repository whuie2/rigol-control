//! Structures for interacting with Rigol RF generators.
//!
//! See official manuals and associated programming guides for more details.

#[allow(unused_imports)]

use std::{
    io::{ BufReader, BufRead, Read, Write },
    time::Duration,
};
use visa_rs::{
    DefaultRM,
    Instrument,
    ResList,
    VisaString,
    flags::AccessMode,
};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum RigolError {
    #[error("error finding available resources: {0}")]
    FailedFindResource(String),

    #[error("couldn't get a handle to the default resource manager: {0}")]
    FailedRMHandle(String),

    #[error("invalid search expression '{0}'")]
    InvalidSearchExpr(String),

    #[error("invalid device ID '{0}'")]
    InvalidDeviceID(String),

    #[error("couldn't connect to Rigol device: {0}")]
    FailedConnect(String),

    #[error("invalid channel number {0}")]
    InvalidChannel(usize),

    #[error("couldn't parse response as a number: '{0}'")]
    InvalidRespNumber(String),

    #[error("serial communication error: {0}")]
    SerialCommunicationError(#[from] std::io::Error),
}
pub type RigolResult<T> = Result<T, RigolError>;

/// List all available resources, matched against `search_expr`. If no search
/// expression is provided, list every available resource.
///
/// See [the official documentation][visa-search] on the search expression
/// language.
///
/// [visa-search]: https://www.ni.com/docs/en-US/bundle/ni-visa-api-ref/page/ni-visa-api-ref/vifindrsrc.html
pub fn list_visa_resources(search_expr: Option<&str>)
    -> RigolResult<Vec<String>>
{
    let search_expr: &str = search_expr.unwrap_or("?*");
    let resource_manager = DefaultRM::new()
        .map_err(|err| RigolError::FailedRMHandle(err.to_string()))?;
    let mut res_list: ResList
        = resource_manager.find_res_list(
            &VisaString::from_string(search_expr.to_string())
                .ok_or(RigolError::InvalidSearchExpr(search_expr.to_string()))?
        )
        .map_err(|err| RigolError::FailedFindResource(err.to_string()))?;
    let mut acc: Vec<String> = Vec::new();
    while let Some(res)
        = res_list.find_next()
        .map_err(|err| RigolError::FailedFindResource(err.to_string()))?
    {
        acc.push(
            res.to_string_lossy()
                .into_owned()
                .to_string()
        );
    }
    return Ok(acc);
}

/// Main driver to handle communication with a Rigol DG800-series RF signal
/// generator.
pub struct DG800 {
    resource_manager: DefaultRM,
    dev: BufReader<Instrument>,
}

impl std::fmt::Debug for DG800 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return write!(f, "DG800");
    }
}

impl DG800 {
    /// Connect to a VISA device with the given ID, creating a new instance of
    /// `Self`.
    ///
    /// This connection is exclusive, and all other attempts to connect to the
    /// same device will fail.
    pub fn connect(id: &str, connect_timeout_sec: Option<f64>)
        -> RigolResult<Self>
    {
        let resource_name = VisaString::from_string(id.to_string())
            .ok_or(RigolError::InvalidDeviceID(id.to_string()))?;
        let resource_manager = DefaultRM::new()
            .map_err(|err| RigolError::FailedRMHandle(err.to_string()))?;
        let dev: Instrument
            = resource_manager.open(
                &resource_name,
                AccessMode::EXCLUSIVE_LOCK,
                Duration::from_secs_f64(connect_timeout_sec.unwrap_or(1.0)),
            )
            .map_err(|err| RigolError::FailedConnect(err.to_string()))?;
        return Ok(Self { resource_manager, dev: BufReader::new(dev) });
    }

    /// Disconnect from all the device and associated manager, dropping `self`.
    pub fn disconnect(self) { }

    fn check_channel(ch: usize) -> RigolResult<usize> {
        return (1..=2).contains(&ch).then_some(ch)
            .ok_or(RigolError::InvalidChannel(ch));
    }

    /// Send a raw string of bytes to the device.
    pub fn send_raw(&mut self, cmd: &[u8]) -> RigolResult<&mut Self> {
        self.dev.get_mut().write_all(cmd)?;
        return Ok(self);
    }

    /// Send `cmd` to the device.
    pub fn send(&mut self, cmd: &str) -> RigolResult<&mut Self> {
        self.dev.get_mut().write_all(cmd.as_bytes())?;
        return Ok(self);
    }

    /// Receive exactly `nbytes` from the device, blocking until all bytes have
    /// been read or an error occurs.
    pub fn recv_raw(&mut self, nbytes: usize) -> RigolResult<Vec<u8>> {
        let mut buffer: Vec<u8> = vec![0; nbytes];
        self.dev.read_exact(&mut buffer)?;
        return Ok(buffer);
    }

    /// Read bytes from the device into a `String` until newline or EOF is
    /// reached. The carriage returns and newline characters will be included.
    pub fn recv(&mut self) -> RigolResult<String> {
        let mut buffer = String::new();
        self.dev.read_line(&mut buffer)?;
        return Ok(buffer);
    }

    /// [`send`][Self::send] followed by [`recv`][Self::recv], returning the
    /// response as a `String`. Leading and trailing whitespace characters will
    /// be trimmed.
    pub fn ask(&mut self, cmd: &str) -> RigolResult<String> {
        self.send(cmd)?;
        let response: String = self.recv()?;
        return Ok(response.trim().to_string());
    }

    /// Get the frequency, in MHz, of a channel.
    pub fn get_frequency(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String = self.ask(&format!(":source{}:frequency?", ch))?;
        return response.parse::<f64>()
            .map(|freq| freq / 1e6)
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the frequency, in MHz, of a channel.
    pub fn set_frequency(&mut self, ch: usize, freq: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        return self.send(&format!(":source{}:frequency {:.3}", ch, freq * 1e6));
    }

    /// Get the amplitude, in Vpp, of a channel.
    pub fn get_amplitude(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String = self.ask(&format!(":source{}:voltage?", ch))?;
        return response.parse::<f64>()
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the amplitude, in Vpp, of a channel.
    pub fn set_amplitude(&mut self, ch: usize, ampl: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        return self.send(&format!(":source{}:voltage {:.4}", ch, ampl));
    }

    /// Get the phase, in degrees, of a channel.
    pub fn get_phase(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String = self.ask(&format!(":source{}:phase?", ch))?;
        return response.parse::<f64>()
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the phase, in degrees, of a channel.
    pub fn set_phase(&mut self, ch: usize, phase: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        return self.send(&format!(":source{}:phase {:.3}", ch, phase));
    }

    /// Get the delay, in seconds, of a channel.
    pub fn get_delay(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String = self.ask(&format!(":source{}:tdelay?", ch))?;
        return response.parse::<f64>()
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the delay, in seconds, of a channel.
    pub fn set_delay(&mut self, ch: usize, delay: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        return self.send(&format!(":source{}:burst:tdelay {:.3}", ch, delay));
    }

    /// Get the period, in seconds, of a channel.
    pub fn get_period(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String
            = self.ask(&format!(":source{}:function:pulse:period?", ch))?;
        return response.parse::<f64>()
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the period, in seconds, of a channel.
    pub fn set_period(&mut self, ch: usize, period: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        let cmd: String
            = format!(":source{}:function:pulse:period {:.9}", ch, period);
        return self.send(&cmd);
    }

    // TODO: trigger/mode/function settings
}

/// Main driver to handle communication with a Rigol DG4000-series RF signal
/// generator.
pub struct DG4000 {
    resource_manager: DefaultRM,
    dev: BufReader<Instrument>,
}

impl std::fmt::Debug for DG4000 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return write!(f, "DG4000");
    }
}

impl DG4000 {
    /// Connect to a VISA device with the given ID, creating a new instance of
    /// `Self`.
    ///
    /// This connection is exclusive, and all other attempts to connect to the
    /// same device will fail.
    pub fn connect(id: &str, connect_timeout_sec: Option<f64>)
        -> RigolResult<Self>
    {
        let resource_name = VisaString::from_string(id.to_string())
            .ok_or(RigolError::InvalidDeviceID(id.to_string()))?;
        let resource_manager = DefaultRM::new()
            .map_err(|err| RigolError::FailedRMHandle(err.to_string()))?;
        let dev: Instrument
            = resource_manager.open(
                &resource_name,
                AccessMode::EXCLUSIVE_LOCK,
                Duration::from_secs_f64(connect_timeout_sec.unwrap_or(1.0)),
            )
            .map_err(|err| RigolError::FailedConnect(err.to_string()))?;
        return Ok(Self { resource_manager, dev: BufReader::new(dev) });
    }

    /// Disconnect from all the device and associated manager, dropping `self`.
    pub fn disconnect(self) { }

    fn check_channel(ch: usize) -> RigolResult<usize> {
        return (1..=2).contains(&ch).then_some(ch)
            .ok_or(RigolError::InvalidChannel(ch));
    }

    /// Send a raw string of bytes to the device.
    pub fn send_raw(&mut self, cmd: &[u8]) -> RigolResult<&mut Self> {
        self.dev.get_mut().write_all(cmd)?;
        return Ok(self);
    }

    /// Send `cmd` to the device.
    pub fn send(&mut self, cmd: &str) -> RigolResult<&mut Self> {
        self.dev.get_mut().write_all(cmd.as_bytes())?;
        return Ok(self);
    }

    /// Receive exactly `nbytes` from the device, blocking until all bytes have
    /// been read or an error occurs.
    pub fn recv_raw(&mut self, nbytes: usize) -> RigolResult<Vec<u8>> {
        let mut buffer: Vec<u8> = vec![0; nbytes];
        self.dev.read_exact(&mut buffer)?;
        return Ok(buffer);
    }

    /// Read bytes from the device into a `String` until newline or EOF is
    /// reached. The carriage returns and newline characters will be included.
    pub fn recv(&mut self) -> RigolResult<String> {
        let mut buffer = String::new();
        self.dev.read_line(&mut buffer)?;
        return Ok(buffer);
    }

    /// [`send`][Self::send] followed by [`recv`][Self::recv], returning the
    /// response as a `String`. Leading and trailing whitespace characters will
    /// be trimmed.
    pub fn ask(&mut self, cmd: &str) -> RigolResult<String> {
        self.send(cmd)?;
        let response: String = self.recv()?;
        return Ok(response.trim().to_string());
    }

    /// Get the frequency, in MHz, of a channel.
    pub fn get_frequency(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String = self.ask(&format!(":source{}:frequency?", ch))?;
        return response.parse::<f64>()
            .map(|freq| freq / 1e6)
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the frequency, in MHz, of a channel.
    pub fn set_frequency(&mut self, ch: usize, freq: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        return self.send(&format!(":source{}:frequency {:.3}", ch, freq * 1e6));
    }

    /// Get the amplitude, in Vpp, of a channel.
    pub fn get_amplitude(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String = self.ask(&format!(":source{}:voltage?", ch))?;
        return response.parse::<f64>()
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the amplitude, in Vpp, of a channel.
    pub fn set_amplitude(&mut self, ch: usize, ampl: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        return self.send(&format!(":source{}:voltage {:.4}", ch, ampl));
    }

    /// Get the phase, in degrees, of a channel.
    pub fn get_phase(&mut self, ch: usize) -> RigolResult<f64> {
        Self::check_channel(ch)?;
        let response: String = self.ask(&format!(":source{}:phase?", ch))?;
        return response.parse::<f64>()
            .map_err(|_| RigolError::InvalidRespNumber(response));
    }

    /// Set the phase, in degrees, of a channel.
    pub fn set_phase(&mut self, ch: usize, phase: f64)
        -> RigolResult<&mut Self>
    {
        Self::check_channel(ch)?;
        return self.send(&format!(":source{}:phase {:.3}", ch, phase));
    }

    // TODO: trigger/mode/function settings
}


