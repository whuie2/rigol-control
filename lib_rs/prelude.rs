pub use crate::{
    rigol::{
        list_visa_resources,
        DG800,
        DG4000,
        RigolError,
        RigolResult,
    },
};
