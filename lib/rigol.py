"""
Class for interacting with Rigol RF generators. See the manual and
associated programming guide for more details.

Requires packages `pyvisa` and `pyvisa-py`.
"""
from __future__ import annotations
import os
import time
import pyvisa

class SCPIController:
    """
    Ancestor class for communication with SCPI instruments.
    """

    @staticmethod
    def list_resources():
        rm = pyvisa.ResourceManager()
        resources = rm.list_resources()
        rm.close()
        return resources

    @classmethod
    def from_resource_index(cls, idx: int):
        return cls(cls.list_resources()[idx])

    def __init__(self, address: str):
        """
        Constructor. Initializes the object, but does not automatically connect.

        Parameters
        ----------
        address : str
            Address of the resource representing the device.
        """
        self.address = address
        self.resource_manager = None
        self.resource = None
        self.is_connected = False
        self._method_passthrough = None

    def open(self, timeout: float=1.0, check: bool=False):
        """
        Open the resource with the stored name. Disconencts and reconnects if
        already connected.

        Parameters
        ----------
        timeout : float = 1.0 (optional)
            Wait `timeout` seconds for a response from the device.
        check : bool = False (optional)
            Perform a test query after forming a connection.
        """
        if self.is_connected:
            self.close()

        self.resource_manager = pyvisa.ResourceManager()
        self.resource \
            = self.resource_manager.open_resource(
                self.address, open_timeout=timeout
            )
        self.resource.timeout = timeout

        if check:
            try:
                self.info = self.query("*IDN?")
            except Exception as err:
                raise RuntimeError("Failed connection check")

        self.is_connected = True
        return self

    def connect(self, timeout: float=1.0, check: bool=False):
        """
        Alias for `open`.
        """
        return self.open(timeout, check)

    def check_connected(self):
        """
        Check that a resource is connected, raising RuntimeError if not.
        """
        if not (
            self.is_connected
            and self.resource_manager is not None
            and self.resource is not None
        ):
            raise RuntimeError("not connected to a resource")

    def close(self):
        if self.is_connected:
            self.resource.close()
            self.resource = None
            self.resource_manager.close()
            self.resource_manager = None
            self.is_connected = False
        return self

    def disconnect(self):
        """
        Alias for `close`.
        """
        return self.close()

    def write(self, cmd: str):
        """
        Write a command to the connected resource. Raises `RuntimeError` if not
        connected.
        """
        self.resource.write(cmd)
        return self

    def send(self, cmd: str):
        """
        Alias for `write`.
        """
        return self.write(cmd)

    def read(self) -> ...:
        """
        Read output from the connected resource. Raises `RuntimeError` if not
        connected.
        """
        return self.resource.read()

    def receive(self) -> ...:
        """
        Alias for `read`.
        """
        return self.receive()

    def query(self, cmd: str) -> ...:
        """
        Write a command to the connected resource, followed immediately by a
        read. Raises `RuntimeError` if not connected.
        """
        return self.resource.query(cmd)

    def ask(self, cmd: str) -> ...:
        """
        Alias for `query`.
        """
        return self.query(cmd)

    def _do_method_passthrough(self, *args, **kwargs) -> ...:
        if self._method_passthrough is None:
            raise Exception("passthrough method is undefined")
        X = self._method_passthrough(*args, **kwargs)
        self._method_passthrough = None
        return self if X is None else X

    def __getattr__(self, attr) -> ...:
        for X in [self.resource, self.resource_manager]:
            if attr in dir(X):
                a = getattr(X, attr)
                if isinstance(a, self.__init__):
                    self._method_passthrough = a
                    return self._do_method_passthrough
                else:
                    return a
        raise AttributeError

class DG800(SCPIController):
    """
    Class to drive commnication with a Rigol DSG800-series RF signal generator.
    All methods that do not have an explicit return type return `self`.
    """

    def _check_source(self, source: int):
        if source not in {1, 2}:
            raise RuntimeError("source must be either 1 or 2")

    def get_frequency(self, source: int) -> float:
        """
        Get the frequency, in MHz, of a source.
        """
        self._check_source(source)
        return float(self.query(f":source{source:.0f}:frequency?")) / 1e6

    def set_frequency(self, source: int, freq: float):
        """
        Set the frequency, in MHz, of a source.
        """
        self._check_source(source)
        return self.write(f":source{source:.0f}:frequency {1e6 * freq:.3f}")

    def get_amplitude(self, source: int) -> float:
        """
        Get the amplitude, in Vpp, of a source.
        """
        self._check_source(source)
        return float(self.query(f":source{source:.0f}:voltage?"))

    def set_amplitude(self, source: int, ampl: float):
        """
        Set the amplitude, in Vpp, of a source.
        """
        self._check_source(source)
        return self.write(f":source{source:.0f}:voltage {ampl:.4f}")

    def get_phase(self, source: int) -> float:
        """
        Get the phase, in degrees, of a source.
        """
        self._check_source(source)
        return float(self.query(f":source{source:.0f}:phase?"))

    def set_phase(self, source: int, phase: float):
        """
        Set the phase, in degrees, of a source.
        """
        self._check_source(source)
        return self.write(f":source{source:.0f}:phase {phase:.3f}")

    def get_delay(self, source: int) -> float:
        """
        Get the delay of a channel, in seconds, of a source
        """
        self._check_source(source)
        return float(self.query(f":source{source:.0f}:tdelay?"))

    def set_delay(self, source: int, delay: float):
        """
        Set the delay, in seconds, of a source
        """
        self._check_source(source)
        return self.write(f":source{source:.0f}:burst:tdelay {delay:.3f}")

    # TODO: trigger/mode/function settings


class DG4000(SCPIController):
    """
    Class to drive communication with a Rigol DG4000-seried RF signal generator.
    All methods that do not have an explicit return type return `self`.
    """

    def _check_source(self, source: int):
        if source not in {1, 2}:
            raise RuntimeError("source must be either 1 or 2")

    def get_frequency(self, source: int) -> float:
        """
        Get the frequency, in MHz, of a source.
        """
        self._check_source(source)
        return float(self.query(f":source{source:.0f}:frequency?")) / 1e6

    def set_frequency(self, source: int, freq: float):
        """
        Set the frequency, in MHz, of a source.
        """
        self._check_source(source)
        return self.write(f":source{source:.0f}:frequency {1e6 * freq:.3f}")

    def get_amplitude(self, source: int) -> float:
        """
        Get the amplitude, in Vpp, of a source.
        """
        self._check_source(source)
        return float(self.query(f":source{source:.0f}:voltage?"))

    def set_amplitude(self, source: int, ampl: float):
        """
        Set the amplitude, in Vpp, of a source.
        """
        self._check_source(source)
        return self.write(f":source{source:.0f}:voltage {ampl:.4f}")

    def get_phase(self, source: int) -> float:
        """
        Get the phase, in degrees, of a source.
        """
        self._check_source(source)
        return float(self.query(f":source{source:.0f}:phase?"))

    def set_phase(self, source: int, phase: float):
        """
        Set the phase, in degrees, of a source.
        """
        self._check_source(source)
        return self.write(f":source{source:.0f}:phase {phase:.3f}")

    # TODO: trigger/mode/function settings

